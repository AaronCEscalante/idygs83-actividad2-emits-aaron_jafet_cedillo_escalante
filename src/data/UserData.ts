import type { IUserInfo } from "@/interfaces/IUserInfo";

const User:IUserInfo [] = [{
    id: 1,
    name: 'Aaron',
    lastname: 'Cedillo Escalante',
    email: 'aaron@gmail.com',
    age: 21
},
{
    id: 2,
    name: 'Odalys',
    lastname: 'Mendez Torres',
    email: 'odalys@gmail.com',
    age: 20   
},
{
    id: 3,
    name: 'Grecia',
    lastname: 'Torres Cedillo',
    email: 'grecia@gmail.com',
    age: 16   
},
{
    id: 4,
    name: 'Milán',
    lastname: 'Mendez Escalante',
    email: 'milan@gmail.com',
    age: 21   
}
]

export default User