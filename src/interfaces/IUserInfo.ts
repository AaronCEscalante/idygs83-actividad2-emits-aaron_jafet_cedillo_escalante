export interface IUserInfo {
    id: number;
    name: string;
    lastname: string;
    email: string;
    age: number;
}